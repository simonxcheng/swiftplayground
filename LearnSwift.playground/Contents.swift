//: Playground - noun: a place where people can play

import UIKit

var str = "Hello, playground"


var dict = ["a":"astring", "b":"bstring"]
print(dict)
var dict2 = dict
dict2["a"] = "mutated a string"

print("Dictionary 1 \(dict)")
print("Dictionary 2 \(dict2)")

var a: [Int] = [1, 2, 3, 4, 5]
var b = a
assert(a==b)
b[2] = 17
print("a=\(a), b= \(b)")

// Equality by value
var pnta: CGPoint = CGPoint(x: 3, y: 5)
var pntb: CGPoint = CGPoint(x: 1, y: 3)

pntb.x += 2
pntb.y += 2

assert(pnta==pntb)

var lista: [Int] = [1, 2, 3]

var listb: [Int] = [3, 2, 1].sort(<)
assert(lista == listb)

////// Temperature - Implement  Equatable ///////////
struct Temperature: Equatable {
    var celsius: Double = 0
    var fahrenheit: Double {
        get { return celsius * 9 / 5 + 32}
        set {celsius = (newValue - 32) * 5 / 9}
    }
}

func ==(lhs: Temperature, rhs: Temperature) -> Bool {
        return lhs.celsius == rhs.celsius
}
///////////////////// End of Temperature //////////////


/////////////////// Circle //////////////////////////////
struct Circle: Equatable {
    var center: CGPoint
    var radius: Double
    init(center: CGPoint, radius: Double) {
        self.center = center
        self.radius = radius
    } }

func ==(lhs: Circle, rhs: Circle) -> Bool {
    return (lhs.center == rhs.center) && (lhs.radius == rhs.radius)
}
/////////////////// End of Circle //////////////////////////////

/////////////////// Polygon //////////////////////////////
struct Polygon: Equatable {
    var corners: [CGPoint] = []
}

func ==(lhs: Polygon, rhs: Polygon) -> Bool {
    return lhs.corners == rhs.corners
}
/////////////////// End of Polygon //////////////////////////////


protocol Drawable {
    func draw()
    func isEqualTo(other: Drawable) -> Bool
}

// Heterogeneous Equality
extension Equatable where Self : Drawable {
    func isEqualTo(other: Drawable) -> Bool {
        guard let o = other as? Self else { return false }
        return self == o
    }
}

extension Polygon: Drawable {
    func draw() {
        let ctx = UIGraphicsGetCurrentContext()
        CGContextMoveToPoint(ctx, corners.last!.x, corners.last!.y)
        for point in corners {
            CGContextAddLineToPoint(ctx, point.x, point.y)
        }
        CGContextClosePath(ctx)
        CGContextStrokePath(ctx)
    }
}

extension Circle: Drawable {
    func draw() {
        let ctx = UIGraphicsGetCurrentContext()
        let arc = CGPathCreateMutable()
        let tempRadius = CGFloat(radius)
        CGPathAddArc(arc, nil, center.x, center.y, tempRadius, 0, 2 * 3.141592, true)
        CGContextAddPath(ctx, arc)
        CGContextStrokePath(ctx)
    }
}

struct Diagram: Drawable {
    var items: [Drawable] = []
    mutating func addItem(item: Drawable) {
        items.append(item)
    }
    
    func draw() {
        for item in items {
            item.draw()
        }
    }
}

extension Diagram : Equatable {}
func == (lhs: Diagram, rhs: Diagram) -> Bool {
    return lhs.items.count == rhs.items.count
        && !zip(lhs.items, rhs.items).contains { !$0.isEqualTo($1) }
}



var doc = Diagram()
var points: [CGPoint] = []

doc.addItem(Polygon())
doc.addItem(Circle(center: CGPoint(x: 10, y: 10), radius: 12.0))
var doc2 = doc
doc2.items[1] = Polygon(corners: points)




//extension Diagram: Equatable {}
//func == (lhs: Diagram, rhs: Diagram) -> Bool {
//    return lhs.items.count == rhs.items.count
//        && !zip(lhs.items, rhs.items).contains { !$0.isEqualTo($1) }
//}






